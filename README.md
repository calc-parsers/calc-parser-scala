# calc-lib - a simple educational calculator parser

calc-lib is a pretty simple and easy to understand general calculator parser and
evaluator.

It takes in generalized calculator strings and evaluates them to expressions.

It's as simple as:

```scala
new CalcLib("3 + 5 - 8 / 2 * 4 ** 2").eval // returns Right(-56)
```

## Docs

calc-lib only takes in a few different types of expressions, no natural log
available here, but for standard operations it should be more than sufficient.

TL/DR:

* Currently only works with raw integers (no floating point) and with the
  following operations:
* `+`  (addition)
* `-`  (subtraction)
* `/`  (division)
* `*`  (multiplication)
* `**` (powerOf)
* `()` (nested sub-expressions, parenthesis)
* And accepts any numbers smaller than Scala signed int type.

### Some examples of usage:

```scala
new CalcLib("25/5").eval // returns Right(5)
new CalcLib("23 + 99 - 2222 / 1111 * 100 ** 2").eval // returns Right(-19878)
new CalcLib("((((((5 + 6) + 7) + 8) + 9) + 10) + 11)").eval // returns Right(56)
new CalcLib("3 + 5 - 8 / 2 * 4 ** 2").eval //returns  Right(-56)
```

Note that it is not whitespace sensitive, but other characters will cause it to
get a little confused. Perhaps this will be fixed in the future, but it is not a
priority.

Note that lack of whitespace sensitivity includes between literals, so
`new CalcLib("5 5").eval` will return `Right(55)`

## How does it work?

I wrote a blog post about its design, check it out here :D https://blog.darrien.dev/posts/writing-calc-parser/

## Disclosure

calc-lib makes no guarantees about being the best, fastest, or even the most
well written, just that it's written out in easy to understand steps. Written
for educational purposes for the author (Darrien Glasser) and anyone else
interested in learning about calculator parsers.

The code isn't properly namespaced with a com.blah.package.handler for this
reason, so it is not ready to be used in production.

There are only a few classes to move around, so if you decide it is fit for
production, just make sure to namespace everything!

If there is one thing calc-lib has, it's that it is quite well tested. Each
phase and the overall expression compiler has a number of complex tests to
ensure valid input->output. You can be reasonably sure everything works as
advertised :)
