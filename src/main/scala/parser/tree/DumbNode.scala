package parser.tree
import play.api.libs.json._

/** A simple and dumb representation of a tree.
  *
  * @param nodeType Simple typeclass representing what the node type is.
  * @param value Value of node or [[Empty]] if type is an operation.
  * @param lhs Optional left hand child if the node is an operation.
  * @param rhs Optional right hand child if the node is an operation.
  */
sealed case class DumbNode(
    var nodeType: Type,
    var value: NodeValue = Empty(),
    var lhs: Option[DumbNode] = None,
    var rhs: Option[DumbNode] = None
) {
  implicit val implicitNodeWrites = Json.writes[DumbNode]
  implicit val implicitNodeReads  = Json.reads[DumbNode]

  def priority: Int                        = nodeType match {
    case DumbLiteral        => 1
    case DumbParenthesis    => 1
    case DumbPowerOf        => 2
    case DumbMultiplication => 3
    case DumbDivision       => 3
    case DumbAddition       => 4
    case DumbSubtraction    => 4
  }
  def shouldSwap(other: DumbNode): Boolean = other.priority >= priority
  def isHydrated: Boolean                  = lhs.isDefined && rhs.isDefined

  /** Replaces current node parameters with passed in node parameters.
    *
    * @param toSwap Node parameters to put in current node.
    * @return A new node with the old node's parameters.
    */
  def swap(toSwap: DumbNode): DumbNode = {
    val oldNodeType = nodeType
    val oldValue    = value
    val oldLhs      = lhs
    val oldRhs      = rhs

    nodeType = toSwap.nodeType
    value = toSwap.value
    lhs = toSwap.lhs
    rhs = toSwap.rhs

    DumbNode(oldNodeType, oldValue, oldLhs, oldRhs)
  }
  override def toString: String = {
    val json = Json.toJson(this)
    Json.prettyPrint(json)
  }
}

sealed trait Type
case object DumbLiteral        extends Type
case object DumbAddition       extends Type
case object DumbSubtraction    extends Type
case object DumbMultiplication extends Type
case object DumbDivision       extends Type
case object DumbPowerOf        extends Type
case object DumbParenthesis    extends Type

object Type {
  implicit val format: OFormat[Type] = {
    implicit def jsl = Json.format[DumbLiteral.type]
    implicit def jsa = Json.format[DumbAddition.type]
    implicit def jss = Json.format[DumbSubtraction.type]
    implicit def jsm = Json.format[DumbMultiplication.type]
    implicit def jsd = Json.format[DumbDivision.type]
    implicit def jsp = Json.format[DumbPowerOf.type]
    implicit def jsz = Json.format[DumbParenthesis.type]

    Json.format[Type]
  }
}

sealed trait NodeValue
case class LiteralValue(value: Int)     extends NodeValue
case class ParenValue(value: DumbNode)  extends NodeValue
case class Empty(ignored: String = " ") extends NodeValue

object NodeValue {
  implicit val format: OFormat[NodeValue] = {
    implicit val implicitNodeWrites = Json.writes[DumbNode]
    implicit val implicitNodeReads  = Json.reads[DumbNode]

    implicit def nlv = Json.format[LiteralValue]
    implicit def npv = Json.format[ParenValue]
    implicit def ne  = Json.format[Empty]

    Json.format[NodeValue]
  }
}
