package parser

import parser.tree.DumbNode
import parser.tree.DumbAddition
import parser.tree.DumbSubtraction
import parser.tree.DumbLiteral
import parser.tree.LiteralValue
import parser.tree.DumbParenthesis
import parser.tree.ParenValue
import parser.tree.DumbPowerOf
import parser.tree.DumbMultiplication
import parser.tree.DumbDivision

trait Pattern {
  def eval: Int
}

case class Literal(value: Int)                        extends Pattern {
  def eval: Int = value
}
case class Addition(lhs: Pattern, rhs: Pattern)       extends Pattern {
  def eval: Int = lhs.eval + rhs.eval
}
case class Subtraction(lhs: Pattern, rhs: Pattern)    extends Pattern {
  def eval: Int = lhs.eval - rhs.eval
}
case class Multiplication(lhs: Pattern, rhs: Pattern) extends Pattern {
  def eval: Int = lhs.eval * rhs.eval
}
case class Division(lhs: Pattern, rhs: Pattern)       extends Pattern {
  def eval: Int = lhs.eval / rhs.eval
}
case class PowerOf(lhs: Pattern, rhs: Pattern)        extends Pattern {
  def eval: Int = math.pow(lhs.eval, rhs.eval).asInstanceOf[Int]
}

case class Parenthesis(innerPattern: Pattern) extends Pattern {
  def eval: Int = innerPattern.eval
}

/** Validates tree from root node and converts it to evaluation ready tree.
  * Checks validity of paremeters and throws if parameters are invalid.
  *
  * @param dumbRoot Root of simple AST created in [[AstBuilder]] to be validated.
  */
class AstValidator(dumbRoot: DumbNode) {
  def validate: Either[CompileError, Pattern] = validateInternal(dumbRoot)

  private[this] def validateInternal(
      dumbNode: DumbNode
  ): Either[CompileError, Pattern] = {
    dumbNode.nodeType match {
      case DumbLiteral => {
        if (dumbNode.lhs.isDefined || dumbNode.rhs.isDefined) {
          return Left(HasTooManyChildren())
        } else {
          dumbNode.value match {
            case LiteralValue(value) => Right(Literal(value))
            case _                   => Left(InternalError())
          }
        }
      }

      case DumbParenthesis    =>
        dumbNode.value match {
          case ParenValue(value) => {
            validateInternal(value) match {
              case Left(error)  => Left(error)
              case Right(value) => Right(Parenthesis(value))
            }
          }
          case _                 => ???
        }
      case DumbPowerOf        =>
        return if (dumbNode.lhs.isEmpty || dumbNode.rhs.isEmpty) {
          Left(DoesNotHaveRequiredChildren())
        } else {
          nodeMatchers(
            validateInternal(dumbNode.lhs.get),
            validateInternal(dumbNode.rhs.get)
          ) match {
            case MultiError(error1, error2) => Left(MultipleError())
            case SingleError(error)         => Left(error)
            case NoError(result1, result2)  => Right(PowerOf(result1, result2))
          }
        }
      case DumbMultiplication =>
        if (dumbNode.lhs.isEmpty || dumbNode.rhs.isEmpty) {
          Left(DoesNotHaveRequiredChildren())
        } else {
          nodeMatchers(
            validateInternal(dumbNode.lhs.get),
            validateInternal(dumbNode.rhs.get)
          ) match {
            case MultiError(error1, error2) => Left(MultipleError())
            case SingleError(error)         => Left(error)
            case NoError(result1, result2)  =>
              Right(Multiplication(result1, result2))
          }
        }
      case DumbDivision       =>
        if (dumbNode.lhs.isEmpty || dumbNode.rhs.isEmpty) {
          Left(DoesNotHaveRequiredChildren())
        } else {
          nodeMatchers(
            validateInternal(dumbNode.lhs.get),
            validateInternal(dumbNode.rhs.get)
          ) match {
            case MultiError(error1, error2) => Left(MultipleError())
            case SingleError(error)         => Left(error)
            case NoError(result1, result2)  => Right(Division(result1, result2))
          }
        }
      case DumbAddition       =>
        if (dumbNode.lhs.isEmpty || dumbNode.rhs.isEmpty) {
          Left(DoesNotHaveRequiredChildren())
        } else {
          nodeMatchers(
            validateInternal(dumbNode.lhs.get),
            validateInternal(dumbNode.rhs.get)
          ) match {
            case MultiError(error1, error2) => Left(MultipleError())
            case SingleError(error)         => Left(error)
            case NoError(result1, result2)  => Right(Addition(result1, result2))
          }
        }
      case DumbSubtraction    =>
        if (dumbNode.lhs.isEmpty || dumbNode.rhs.isEmpty) {
          Left(DoesNotHaveRequiredChildren())
        } else {
          nodeMatchers(
            validateInternal(dumbNode.lhs.get),
            validateInternal(dumbNode.rhs.get)
          ) match {
            case MultiError(error1, error2) => Left(MultipleError())
            case SingleError(error)         => Left(error)
            case NoError(result1, result2)  =>
              Right(Subtraction(result1, result2))
          }
        }
    }
  }
  private[this] def nodeMatchers(
      maybeNode1: Either[CompileError, Pattern],
      maybeNode2: Either[CompileError, Pattern]
  ): MultiMatcher[Pattern] = {
    val node1Error = maybeNode1.isLeft
    val node2Error = maybeNode2.isLeft

    if (node1Error && node2Error) {
      MultiError(maybeNode1.swap.getOrElse(???), maybeNode2.swap.getOrElse(???))
    } else if (node1Error || node2Error) {
      if (node1Error) { SingleError(maybeNode1.swap.getOrElse(???)) }
      else { SingleError(maybeNode2.swap.getOrElse(???)) }
    } else { NoError(maybeNode1.getOrElse(???), maybeNode2.getOrElse(???)) }
  }
}

sealed trait MultiMatcher[T]
sealed case class SingleError[T](error: CompileError) extends MultiMatcher[T]
sealed case class MultiError[T](error1: CompileError, error2: CompileError)
    extends MultiMatcher[T]
sealed case class NoError[T](result1: T, result2: T)  extends MultiMatcher[T]
