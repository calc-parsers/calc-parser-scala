package parser

import scala.collection.mutable.ListBuffer
import scala.collection.mutable.Stack
import util.control.Breaks._

abstract class RawToken
case class RawLiteral(value: Int)                        extends RawToken
case class RawAddition()                                 extends RawToken
case class RawSubtraction()                              extends RawToken
case class RawMultiplication()                           extends RawToken
case class RawDivision()                                 extends RawToken
case class RawPowerOf()                                  extends RawToken
case class RawParenthesis(innerPatterns: List[RawToken]) extends RawToken

/** Simple calculator parser. Turns string into raw, abstract tokens.
  * Tokens are very mostly stored in a flat list, with the exception of
  * parenthesis. Parenthesis contain their own sub-list of tokens.
  *
  * Checks for validity of number of parenthesis.
  *
  * @param rawPatterns Raw string to parse into abstract tokens.
  */
class Parser(rawPatterns: String) {
  def parse: Either[CompileError, List[RawToken]] = {
    parseInternal(rawPatterns, Stack())._2
  }

  private[this] def parseInternal(
      rawPatterns: String,
      parenStack: Stack[Unit]
  ): (Int, Either[CompileError, List[RawToken]]) = {
    val localPatterns: ListBuffer[RawToken] = new ListBuffer()
    var ignoreTo                            = -1
    for ((c, index) <- rawPatterns.view.zipWithIndex) {
      breakable {
        // We handle inner paren data in a recursive parent, we should ignore all input until
        // we get to a closing paren and handle it there
        if (index <= ignoreTo) { break() }
        c match {
          case ' ' => ()
          case '+' => localPatterns.append(RawAddition())
          case '-' => localPatterns.append(RawSubtraction())
          case '/' => localPatterns.append(RawDivision())
          case '*' =>
            getRawForMult(localPatterns.toList) match {
              case RawMultiplication() =>
                localPatterns
                  .append(RawMultiplication())
              case RawPowerOf()        => {
                localPatterns.dropRightInPlace(1)
                localPatterns.append(RawPowerOf())
              }
              case _                   => ???
            }
          case '(' => {
            parenStack.push(())
            val (updatedIgnore, parenCalc) =
              handleOpenParen(rawPatterns, index, parenStack)
            if (parenCalc.isLeft) {
              return (index, Left(parenCalc.swap.getOrElse(UnknownError())))
            }
            localPatterns.append(parenCalc.getOrElse(???))
            ignoreTo = updatedIgnore.max(ignoreTo)
          }
          case ')' => {
            try {
              parenStack.pop()
              return (index, Right(localPatterns.toList))
            } catch {
              // too many close parens
              case e: NoSuchElementException =>
                return (index, Left(MismatchedParens()))
            }
          }
          case _   =>
            val result = handleNumber(c)
            if (result.isLeft) {
              return (index, Left(result.swap.getOrElse(UnknownError())))
            }
            updateNumberLiteralIfApplicable(
              result.getOrElse(???),
              localPatterns
            )
        }
      }
    }

    if (parenStack.size > 0) {
      // too many open parens
      (-1, Left(MismatchedParens()))
    } else { (-1, Right(localPatterns.toList)) }
  }

  private[this] def updateNumberLiteralIfApplicable(
      newNumber: RawLiteral,
      patterns: ListBuffer[RawToken]
  ): Unit = {
    try {
      patterns.last match {
        case RawLiteral(value) => {
          patterns.dropRightInPlace(1)
          patterns.append(RawLiteral(value * 10 + newNumber.value))
        }
        case _                 => patterns.append(newNumber)
      }
    } catch { case e: NoSuchElementException => patterns.append(newNumber) }
  }

  private[this] def handleNumber(
      char: Char
  ): Either[CompileError, RawLiteral] = {
    val numericVal = char.getNumericValue
    return numericVal match {
      case x if x > 9 || x < 0 => Left(InvalidNumber(char))
      case _                   => Right(RawLiteral(numericVal))
    }
  }

  private[this] def handleOpenParen(
      rawPatterns: String,
      index: Int,
      parenStack: Stack[Unit]
  ): (Int, Either[CompileError, RawParenthesis]) = {
    val shortenedPattern         = rawPatterns.substring(index + 1)
    val (ignoreRange, parenCalc) = parseInternal(shortenedPattern, parenStack)

    (ignoreRange + index + 1, parenCalc.map((tokens) => RawParenthesis(tokens)))
  }

  private[this] def getRawForMult(localList: List[RawToken]): RawToken = {
    try {
      localList.last match {
        case RawMultiplication() => RawPowerOf()
        case _                   => RawMultiplication()
      }
    } catch { case e: NoSuchElementException => RawMultiplication() }
  }
}
