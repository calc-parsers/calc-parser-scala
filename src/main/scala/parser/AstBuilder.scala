package parser

import parser.tree.DumbNode
import parser.tree.DumbLiteral
import parser.tree.DumbAddition
import parser.tree.DumbSubtraction
import parser.tree.DumbParenthesis
import parser.tree.LiteralValue
import parser.tree.ParenValue
import parser.tree.Empty
import parser.tree.DumbDivision
import parser.tree.DumbPowerOf
import parser.tree.DumbMultiplication

/** Converts raw tokens to simple, but dumb AST. AST is not validated to
  * ensure validity of tokens and simply acts as if all tokens are in valid
  * places. This means if the token list (as string) is given:
  * 5 + 6 7
  * The tree will act as if 7 is an operation to be run on 6 and will make
  * 6 the child even though literals cannot have children.
  *
  * In this phase, we ensure order of operations is adhered to with a simple
  * priority system (see [[tree.DumbNode]] for details on priority).
  *
  * @param rawTokens List of tokens to convert to tree form.
  */
class AstBuilder(rawTokens: List[RawToken]) {
  def build: Either[CompileError, DumbNode] = { buildInternal(rawTokens) }

  private[this] def buildInternal(
      rawTokens: List[RawToken]
  ): Either[CompileError, DumbNode] = {
    // no tokens is an invalid sequence
    if (rawTokens.size == 0) { return Left(NoTokens()) }

    var rootPattern: Option[DumbNode]   = None
    // last operation == every other token => 1, 3, 5, 7
    // if the equation is properly formed, these will ALWAYS be nodes that have 2 children (add, sub, etc.)
    var lastOperation: Option[DumbNode] = None

    for ((rawToken, idx) <- rawTokens.zipWithIndex) {
      val maybeNode = rawTokenToPattern(rawToken)
      maybeNode match {
        case Left(error)    => return Left(error)
        case Right(newNode) => {
          rootPattern match {
            case None           => rootPattern = Some(newNode);
            case Some(rootNode) => {
              lastOperation match {
                case None                     => {
                  val previousRoot = rootNode.swap(newNode)
                  rootNode.lhs = Some(previousRoot)
                  lastOperation = Some(rootNode)
                }
                case Some(lastOperationValue) => {
                  val (newLastOp, newRoot) = handleSomeLastValue(
                    lastOperationValue,
                    newNode,
                    rootNode,
                    idx
                  )
                  rootPattern = newRoot
                  lastOperation = newLastOp
                }
              }
            }
          }
        }
      }
    }
    Right(rootPattern.get)
  }

  private[this] def handleSomeLastValue(
      lastOperationValue: DumbNode,
      newNode: DumbNode,
      rootNode: DumbNode,
      idx: Int
  ): (Option[DumbNode], Option[DumbNode]) = {
    val lastIsRoot = lastOperationValue.eq(rootNode)

    if (idx % 2 == 0) {
      lastOperationValue.rhs = Some(newNode)
      (Some(lastOperationValue), Some(rootNode))
    } else {
      if (lastOperationValue.shouldSwap(newNode)) {
        val previousLastOperation = lastOperationValue.swap(newNode)
        lastOperationValue.lhs = Some(previousLastOperation)
        val wrappedLast           = Some(lastOperationValue)
        if (lastIsRoot) { (wrappedLast, wrappedLast) }
        else { (wrappedLast, Some(rootNode)) }
      } else {
        if (lastOperationValue.isHydrated) {
          val oldRhs = lastOperationValue.rhs.get.swap(newNode)
          lastOperationValue.rhs.get.lhs = Some(oldRhs)
          (lastOperationValue.rhs, Some(rootNode))
        } else {
          val wrappedNewNode = Some(newNode)
          lastOperationValue.rhs = wrappedNewNode
          (wrappedNewNode, Some(rootNode))
        }
      }
    }
  }

  private[this] def rawTokenToPattern(
      rawToken: RawToken
  ): Either[CompileError, DumbNode] = {
    rawToken match {
      case RawLiteral(value)      =>
        Right(DumbNode(DumbLiteral, LiteralValue(value)))
      case RawAddition()          => Right(DumbNode(DumbAddition))
      case RawSubtraction()       => Right(DumbNode(DumbSubtraction))
      case RawMultiplication()    => Right(DumbNode(DumbMultiplication))
      case RawDivision()          => Right(DumbNode(DumbDivision))
      case RawPowerOf()           => Right(DumbNode(DumbPowerOf))
      case RawParenthesis(tokens) =>
        buildInternal(tokens) match {
          case Left(error)   => Left(error)
          case Right(tokens) =>
            Right(DumbNode(DumbParenthesis, ParenValue(tokens)))
        }

    }
  }
}
