package parser

sealed trait CompileError
sealed case class InvalidNumber(c: Char)        extends CompileError
sealed case class MismatchedParens()            extends CompileError
sealed case class NoTokens()                    extends CompileError
sealed case class HasTooManyChildren()          extends CompileError
sealed case class DoesNotHaveRequiredChildren() extends CompileError
sealed case class MultipleError()               extends CompileError
sealed case class InternalError()               extends CompileError
sealed case class UnknownError()                extends CompileError
