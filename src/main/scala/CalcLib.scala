import parser.Parser
import parser.AstBuilder
import parser.AstValidator
import parser.CompileError

/** Simple calculator string to parser evaluator.
  * Currently only works with raw integers (no floating point) and with
  * the following operations:
  * +  (addition)
  * -  (subtraction)
  * /  (division)
  * *  (multiplication)
  * ** (powerOf)
  * () (nested sub-expressions, parenthesis)
  * And accepts any numbers smaller than Scala signed int type.
  *
  * @param calculation String to perform evaluation of.
  */
class CalcLib(calculation: String) {
  def eval: Either[CompileError, Int] = {
    new Parser(calculation).parse
      .flatMap(tokens => new AstBuilder(tokens).build)
      .flatMap(simpleAst => {
        new AstValidator(simpleAst).validate

      })
      .flatMap(validatedAst => Right(validatedAst.eval))
  }
}
