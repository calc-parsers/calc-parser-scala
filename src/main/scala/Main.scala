import scala.io.StdIn.readLine

object Main extends App {
  println("Started calculator parser")
  println("type ^c or q to quit")
  while (true) {
    val input  = readLine("calc> ").stripLineEnd
    if (input.length() == 1 && input.charAt(0) == 'q') {
      System.exit(0)
    }
    val output = new CalcLib(input).eval
    output match {
      case Left(error)  => println(s"err evaluating: $error")
      case Right(value) => println(value)
    }
  }
}
