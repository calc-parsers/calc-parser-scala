import org.scalatest._
import funsuite._
import parser.InvalidNumber
import parser.MismatchedParens
import parser.DoesNotHaveRequiredChildren
import parser.NoTokens
import parser.HasTooManyChildren

class CalcLibTests extends AnyFunSuite {
  test("it evaluates simple addition") {
    assert(new CalcLib("5 + 5").eval.getOrElse(???) == 10)
  }
  test("it evaluates simple subtraction") {
    assert(new CalcLib("5 - 5").eval.getOrElse(???) == 0)
  }
  test("it evaluates simple multiplication") {
    assert(new CalcLib("5 * 5").eval.getOrElse(???) == 25)
  }
  test("it evaluates simple division") {
    assert(new CalcLib("25/5").eval.getOrElse(???) == 5)
  }
  test("it evaluates simple power of") {
    assert(new CalcLib("2 ** 5").eval.getOrElse(???) == 32)
  }
  test("it evaluates multiple additions") {
    assert(new CalcLib("2 + 3 + 4 + 5").eval.getOrElse(???) == 14)
  }
  test("it evaluates order of operations") {
    assert(new CalcLib("3 + 5 - 8 / 2 * 4 ** 2").eval.getOrElse(???) == -56)
  }
  test("it evaluates multi-digit additions") {
    assert(new CalcLib("54321 + 90").eval.getOrElse(???) == 54411)
  }
  test("it evaluates multi-digit order of operations") {
    assert(
      new CalcLib("23 + 99 - 2222 / 1111 * 100 ** 2").eval.getOrElse(???) ==
        -19878
    )
  }
  test("it evaluates sub-expressions") {
    assert(new CalcLib("(5 + 5)").eval.getOrElse(???) == 10)
  }
  test("it evaluates nested sub-expressions") {
    assert(new CalcLib("((5 + 5))").eval.getOrElse(???) == 10)
  }
  test("it evaluates sub-expressions and regular expressions") {
    assert(
      new CalcLib("(18 + 6) / 12 - (22 ** 3)").eval.getOrElse(???) == -10646
    )
  }
  test("it evaluates high complexity sub-expressions") {
    assert(
      new CalcLib("((((((5 + 6) + 7) + 8) + 9) + 10) + 11)").eval
        .getOrElse(???) == 56
    )
  }
  test("it throws error on invalid number") {
    assert(new CalcLib("5 + x").eval.swap.getOrElse(???) === InvalidNumber('x'))
  }
  test("it throws error on invalid parens") {
    assert(
      new CalcLib("((5 + 3)").eval.swap.getOrElse(???) === MismatchedParens()
    )
  }
  test("it throws error on operator missing children") {
    assert(
      new CalcLib("5 + +").eval.swap.getOrElse(???) ===
        DoesNotHaveRequiredChildren()
    )
  }
  test("it throws error when there are no tokens") {
    assert(new CalcLib("").eval.swap.getOrElse(???) === NoTokens())
  }
}
