import org.scalatest.funsuite.AnyFunSuite
import parser.AstBuilder
import parser.RawToken
import parser.RawLiteral
import parser.RawAddition
import parser.tree.DumbNode
import parser.tree.Empty
import parser.tree.DumbAddition
import parser.tree.DumbLiteral
import parser.tree.LiteralValue
import parser.RawSubtraction
import parser.tree.DumbSubtraction
import parser.RawMultiplication
import parser.tree.DumbMultiplication
import parser.RawDivision
import parser.tree.DumbDivision
import parser.RawPowerOf
import parser.tree.DumbPowerOf
import parser.RawParenthesis
import parser.tree.DumbParenthesis
import parser.tree.ParenValue
import parser.NoTokens

class AstBuilderTests extends AnyFunSuite {
  test("it builds simple addition to dumb ast") {
    assert(
      new AstBuilder(List(RawLiteral(5), RawAddition(), RawLiteral(6))).build
        .getOrElse(???) === DumbNode(
        DumbAddition,
        Empty(),
        Some(DumbNode(DumbLiteral, LiteralValue(5))),
        Some(DumbNode(DumbLiteral, LiteralValue(6)))
      )
    )
  }
  test("it builds simple subtraction to dumb ast") {
    assert(
      new AstBuilder(List(RawLiteral(6), RawSubtraction(), RawLiteral(2))).build
        .getOrElse(???) === DumbNode(
        DumbSubtraction,
        Empty(),
        Some(DumbNode(DumbLiteral, LiteralValue(6))),
        Some(DumbNode(DumbLiteral, LiteralValue(2)))
      )
    )
  }
  test("it parses simple multiplication to dumb ast") {
    assert(
      new AstBuilder(
        List(RawLiteral(6), RawMultiplication(), RawLiteral(9))
      ).build.getOrElse(???) === DumbNode(
        DumbMultiplication,
        Empty(),
        Some(DumbNode(DumbLiteral, LiteralValue(6))),
        Some(DumbNode(DumbLiteral, LiteralValue(9)))
      )
    )
  }
  test("it builds simple division to dumb ast") {
    assert(
      new AstBuilder(List(RawLiteral(5), RawDivision(), RawLiteral(2))).build
        .getOrElse(???) === DumbNode(
        DumbDivision,
        Empty(),
        Some(DumbNode(DumbLiteral, LiteralValue(5))),
        Some(DumbNode(DumbLiteral, LiteralValue(2)))
      )
    )
  }
  test("it builds multi-additions to dumb ast") {
    assert(
      new AstBuilder(
        List(
          RawLiteral(2),
          RawAddition(),
          RawLiteral(5),
          RawAddition(),
          RawLiteral(6),
          RawAddition(),
          RawLiteral(0)
        )
      ).build.getOrElse(???) === DumbNode(
        DumbAddition,
        Empty(),
        Some(
          DumbNode(
            DumbAddition,
            Empty(),
            Some(
              DumbNode(
                DumbAddition,
                Empty(),
                Some(DumbNode(DumbLiteral, LiteralValue(2))),
                Some(DumbNode(DumbLiteral, LiteralValue(5)))
              )
            ),
            Some(DumbNode(DumbLiteral, LiteralValue(6)))
          )
        ),
        Some(DumbNode(DumbLiteral, LiteralValue(0)))
      )
    )
  }
  test("it builds in order of operations properly to dumb ast") {
    assert(
      new AstBuilder(
        List(
          RawLiteral(2),
          RawAddition(),
          RawLiteral(3),
          RawMultiplication(),
          RawLiteral(5)
        )
      ).build.getOrElse(???) === DumbNode(
        DumbAddition,
        Empty(),
        Some(DumbNode(DumbLiteral, LiteralValue(2))),
        Some(
          DumbNode(
            DumbMultiplication,
            Empty(),
            Some(DumbNode(DumbLiteral, LiteralValue(3))),
            Some(DumbNode(DumbLiteral, LiteralValue(5)))
          )
        )
      )
    )
  }
  test("it handles complex multi-expressions to dumb ast") {
    assert(
      new AstBuilder(
        List(
          RawLiteral(3),
          RawAddition(),
          RawLiteral(5),
          RawSubtraction(),
          RawLiteral(9),
          RawDivision(),
          RawLiteral(2),
          RawMultiplication(),
          RawLiteral(4),
          RawPowerOf(),
          RawLiteral(1)
        )
      ).build.getOrElse(???) === DumbNode(
        DumbSubtraction,
        Empty(),
        Some(
          DumbNode(
            DumbAddition,
            Empty(),
            Some(DumbNode(DumbLiteral, LiteralValue(3))),
            Some(DumbNode(DumbLiteral, LiteralValue(5)))
          )
        ),
        Some(
          DumbNode(
            DumbMultiplication,
            Empty(),
            Some(
              DumbNode(
                DumbDivision,
                Empty(),
                Some(DumbNode(DumbLiteral, LiteralValue(9))),
                Some(DumbNode(DumbLiteral, LiteralValue(2)))
              )
            ),
            Some(
              DumbNode(
                DumbPowerOf,
                Empty(),
                Some(DumbNode(DumbLiteral, LiteralValue(4))),
                Some(DumbNode(DumbLiteral, LiteralValue(1)))
              )
            )
          )
        )
      )
    )
  }
  test("it handles building sub-expressions to dumb ast") {
    assert(
      new AstBuilder(
        List(
          RawParenthesis(List(RawLiteral(5), RawAddition(), RawLiteral(2)))
        )
      ).build.getOrElse(???) === DumbNode(
        DumbParenthesis,
        ParenValue(
          DumbNode(
            DumbAddition,
            Empty(),
            Some(DumbNode(DumbLiteral, LiteralValue(5))),
            Some(DumbNode(DumbLiteral, LiteralValue(2)))
          )
        )
      )
    )
  }
  test(
    "it handles complex sub-expressions with outer expressions to dumb ast"
  ) {
    assert(
      new AstBuilder(
        List(
          RawParenthesis(List(RawLiteral(5), RawAddition(), RawLiteral(2))),
          RawAddition(),
          RawLiteral(9)
        )
      ).build.getOrElse(???) === DumbNode(
        DumbAddition,
        Empty(),
        Some(
          DumbNode(
            DumbParenthesis,
            ParenValue(
              DumbNode(
                DumbAddition,
                Empty(),
                Some(DumbNode(DumbLiteral, LiteralValue(5))),
                Some(DumbNode(DumbLiteral, LiteralValue(2)))
              )
            )
          )
        ),
        Some(DumbNode(DumbLiteral, LiteralValue(9)))
      )
    )
  }
  test("it returns error if there are no tokens") {
    assert(new AstBuilder(List()).build.swap.getOrElse(???) === NoTokens())
  }
  test("it returns error if there are no tokens in a subexpression") {
    assert(
      new AstBuilder(
        List(RawLiteral(5), RawAddition(), RawParenthesis(List()))
      ).build.swap.getOrElse(???) === NoTokens()
    )
  }
}
