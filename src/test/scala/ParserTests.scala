import org.scalatest._
import funsuite._
import parser.RawLiteral
import parser.RawAddition
import parser.Parser
import parser.RawSubtraction
import parser.RawMultiplication
import parser.RawDivision
import parser.RawPowerOf
import parser.RawParenthesis
import parser.InvalidNumber
import parser.MismatchedParens

class ParserTests extends AnyFunSuite {
  test("it parses simple addition to raw types") {
    assert(
      new Parser("5 + 6").parse.getOrElse(???) ===
        List(RawLiteral(5), RawAddition(), RawLiteral(6))
    )
  }
  test("it parses simple subtraction to raw types") {
    assert(
      new Parser("6 - 2").parse.getOrElse(???) ===
        List(RawLiteral(6), RawSubtraction(), RawLiteral(2))
    )
  }
  test("it parses simple multiplication to raw types") {
    assert(
      new Parser("6 * 9").parse.getOrElse(???) ===
        List(RawLiteral(6), RawMultiplication(), RawLiteral(9))
    )
  }

  test("it parses simple division to raw types") {
    assert(
      new Parser("5 / 2").parse.getOrElse(???) ===
        List(RawLiteral(5), RawDivision(), RawLiteral(2))
    )
  }
  test("it parses simple power of equations") {
    assert(
      new Parser("2 ** 9").parse.getOrElse(???) ===
        List(RawLiteral(2), RawPowerOf(), RawLiteral(9))
    )
  }
  test("it parses simple expression without spaces") {
    assert(
      new Parser("3-3").parse.getOrElse(???) ===
        List(RawLiteral(3), RawSubtraction(), RawLiteral(3))
    )
  }
  test("it parses simple expression with a bunch of spaces") {
    assert(
      new Parser("        2   +                  9").parse.getOrElse(???) ===
        List(RawLiteral(2), RawAddition(), RawLiteral(9))
    )
  }
  test("it parses multi-additions") {
    assert(
      new Parser("2 + 5 + 6 + 0").parse.getOrElse(???) === List(
        RawLiteral(2),
        RawAddition(),
        RawLiteral(5),
        RawAddition(),
        RawLiteral(6),
        RawAddition(),
        RawLiteral(0)
      )
    )
  }
  test("it parses multi-expressions") {
    assert(
      new Parser("3 + 5 - 9 / 2 * 4 ** 1").parse.getOrElse(???) === List(
        RawLiteral(3),
        RawAddition(),
        RawLiteral(5),
        RawSubtraction(),
        RawLiteral(9),
        RawDivision(),
        RawLiteral(2),
        RawMultiplication(),
        RawLiteral(4),
        RawPowerOf(),
        RawLiteral(1)
      )
    )
  }
  test("it parses multi-digit expressions properly") {
    assert(
      new Parser("54321 + 90").parse.getOrElse(???) ===
        List(RawLiteral(54321), RawAddition(), RawLiteral(90))
    )
  }
  test("it parses complex multi-digit expressions properly") {
    assert(
      new Parser("23 + 99 - 2222 / 1223 * 463 ** 12346").parse
        .getOrElse(???) === List(
        RawLiteral(23),
        RawAddition(),
        RawLiteral(99),
        RawSubtraction(),
        RawLiteral(2222),
        RawDivision(),
        RawLiteral(1223),
        RawMultiplication(),
        RawLiteral(463),
        RawPowerOf(),
        RawLiteral(12346)
      )
    )
  }
  test("it parses sub-expression properly") {
    assert(
      new Parser("(5 + 5)").parse.getOrElse(???) ===
        List(RawParenthesis(List(RawLiteral(5), RawAddition(), RawLiteral(5))))
    )
  }
  test("it parses medium complexity sub-expressions properly") {
    assert(
      new Parser("(25 + 15) / (36 ** 2 + 100)").parse.getOrElse(???) === List(
        RawParenthesis(List(RawLiteral(25), RawAddition(), RawLiteral(15))),
        RawDivision(),
        RawParenthesis(
          List(
            RawLiteral(36),
            RawPowerOf(),
            RawLiteral(2),
            RawAddition(),
            RawLiteral(100)
          )
        )
      )
    )
  }
  test("it parses nested sub-expressions properly") {
    assert(
      new Parser("((5 + 5))").parse.getOrElse(???) === List(
        RawParenthesis(
          List(
            RawParenthesis(List(RawLiteral(5), RawAddition(), RawLiteral(5)))
          )
        )
      )
    )
  }
  test("it parses sub-expressions and outer expressions") {
    assert(
      new Parser("((5 + 5) + 5)").parse.getOrElse(???) ===
        List(
          RawParenthesis(
            List(
              RawParenthesis(List(RawLiteral(5), RawAddition(), RawLiteral(5))),
              RawAddition(),
              RawLiteral(5)
            )
          )
        )
    )
  }
  test("it parses sub-expressions and regular expressions properly") {
    assert(
      new Parser("(5 + 6) / 12 - (22 ** 15)").parse.getOrElse(???) === List(
        RawParenthesis(List(RawLiteral(5), RawAddition(), RawLiteral(6))),
        RawDivision(),
        RawLiteral(12),
        RawSubtraction(),
        RawParenthesis(List(RawLiteral(22), RawPowerOf(), RawLiteral(15)))
      )
    )
  }
  test("it parses high complexity sub-expressions properly") {
    assert(
      new Parser("((((((5 + 5) + 5) + 5) + 5) + 5) + 5)").parse
        .getOrElse(???) === List(
        RawParenthesis(
          List(
            RawParenthesis(
              List(
                RawParenthesis(
                  List(
                    RawParenthesis(
                      List(
                        RawParenthesis(
                          List(
                            RawParenthesis(
                              List(RawLiteral(5), RawAddition(), RawLiteral(5))
                            ),
                            RawAddition(),
                            RawLiteral(5)
                          )
                        ),
                        RawAddition(),
                        RawLiteral(5)
                      )
                    ),
                    RawAddition(),
                    RawLiteral(5)
                  )
                ),
                RawAddition(),
                RawLiteral(5)
              )
            ),
            RawAddition(),
            RawLiteral(5)
          )
        )
      )
    )
  }
  test("it catches invalid numbers and returns an error") {
    assert(new Parser("5 + ~").parse.swap.getOrElse(???) == InvalidNumber('~'))
  }
  test("it catches mismatched open parenthesis and returns and error") {
    assert(new Parser("((5)").parse.swap.getOrElse(???) === MismatchedParens())
  }
  test("it catches mismatched close parenthesis and returns and error") {
    assert(new Parser("(10))").parse.swap.getOrElse(???) === MismatchedParens())
  }
}
